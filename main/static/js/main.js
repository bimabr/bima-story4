function goToByScroll(id){
    $('html,body').animate({
        scrollTop: $("#"+id).offset().top
    },'slow');
    return false;
}

$('.nav-about').on('click', function () {
    goToByScroll("about-me");
});

$('.nav-edu').on('click', function () {
    goToByScroll("edu");
});

$('.nav-contact').on('click', function () {
    goToByScroll("contact-me");
});

$('.ke-about').on('click', function () {
    goToByScroll("about-me");
});

$('.ke-edu').on('click', function () {
    goToByScroll("edu");
});

$('.ke-contact').on('click', function () {
    goToByScroll("contact-me");
});

$('.fa-angle-up').on('click', function () {
    goToByScroll("home");
});
